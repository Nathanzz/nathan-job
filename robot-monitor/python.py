from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
  def setupUi(self, MainWindow):
    MainWindow.setObjectName("MainWindow")
    MainWindow.resize(1336, 768)
    icon = QtGui.QIcon()
    icon.addPixmap(QtGui.QPixmap("gambar/ichiro.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    MainWindow.setWindowIcon(icon)
    self.centralwidget = QtWidgets.QWidget(MainWindow)
    self.centralwidget.setObjectName("centralwidget")
    self.label_31 = QtWidgets.QLabel(self.centralwidget)
    self.label_31.setGeometry(QtCore.QRect(260, 70, 781, 561))
    self.label_31.setText("")
    self.label_31.setPixmap(QtGui.QPixmap("gambar/Field.png"))
    self.label_31.setObjectName("label_31")
    self.layoutWidget = QtWidgets.QWidget(self.centralwidget)
    self.layoutWidget.setGeometry(QtCore.QRect(440, 620, 421, 75))
    self.layoutWidget.setObjectName("layoutWidget")
    self.verticalLayout_9 = QtWidgets.QVBoxLayout(self.layoutWidget)
    self.verticalLayout_9.setContentsMargins(0, 0, 0, 0)
    self.verticalLayout_9.setObjectName("verticalLayout_9")
    self.horizontalLayout_6 = QtWidgets.QHBoxLayout()
    self.horizontalLayout_6.setObjectName("horizontalLayout_6")
    self.pushButton = QtWidgets.QPushButton(self.layoutWidget)
    self.pushButton.setObjectName("pushButton")
    self.horizontalLayout_6.addWidget(self.pushButton)
    self.pushButton_2 = QtWidgets.QPushButton(self.layoutWidget)
    self.pushButton_2.setObjectName("pushButton_2")
    self.horizontalLayout_6.addWidget(self.pushButton_2)
    self.pushButton_3 = QtWidgets.QPushButton(self.layoutWidget)
    self.pushButton_3.setObjectName("pushButton_3")
    self.horizontalLayout_6.addWidget(self.pushButton_3)
    self.pushButton_4 = QtWidgets.QPushButton(self.layoutWidget)
    self.pushButton_4.setObjectName("pushButton_4")
    self.horizontalLayout_6.addWidget(self.pushButton_4)
    self.verticalLayout_9.addLayout(self.horizontalLayout_6)
    self.horizontalLayout_5 = QtWidgets.QHBoxLayout()
    self.horizontalLayout_5.setObjectName("horizontalLayout_5")
    self.label_29 = QtWidgets.QLabel(self.layoutWidget)
    font = QtGui.QFont()
    font.setPointSize(25)
    self.label_29.setFont(font)
    self.label_29.setObjectName("label_29")
    self.horizontalLayout_5.addWidget(self.label_29)
    self.label_30 = QtWidgets.QLabel(self.layoutWidget)
    font = QtGui.QFont()
    font.setPointSize(25)
    self.label_30.setFont(font)
    self.label_30.setObjectName("label_30")
    self.horizontalLayout_5.addWidget(self.label_30)
    self.verticalLayout_9.addLayout(self.horizontalLayout_5)
    self.label_32 = QtWidgets.QLabel(self.centralwidget)
    self.label_32.setGeometry(QtCore.QRect(40, 20, 71, 61))
    self.label_32.setText("")
    self.label_32.setPixmap(QtGui.QPixmap("gambar/ichiro.png"))
    self.label_32.setScaledContents(True)
    self.label_32.setObjectName("label_32")
    self.label_33 = QtWidgets.QLabel(self.centralwidget)
    self.label_33.setGeometry(QtCore.QRect(120, 30, 161, 31))
    font = QtGui.QFont()
    font.setPointSize(20)
    self.label_33.setFont(font)
    self.label_33.setObjectName("label_33")
    self.label_40 = QtWidgets.QLabel(self.centralwidget)
    self.label_40.setGeometry(QtCore.QRect(1200, 20, 67, 51))
    self.label_40.setText("")
    self.label_40.setPixmap(QtGui.QPixmap("gambar/Badge_ITS.png"))
    self.label_40.setScaledContents(True)
    self.label_40.setObjectName("label_40")
    self.layoutWidget1 = QtWidgets.QWidget(self.centralwidget)
    self.layoutWidget1.setGeometry(QtCore.QRect(520, 10, 251, 86))
    self.layoutWidget1.setObjectName("layoutWidget1")
    self.verticalLayout_10 = QtWidgets.QVBoxLayout(self.layoutWidget1)
    self.verticalLayout_10.setContentsMargins(0, 0, 0, 0)
    self.verticalLayout_10.setObjectName("verticalLayout_10")
    self.horizontalLayout_7 = QtWidgets.QHBoxLayout()
    self.horizontalLayout_7.setObjectName("horizontalLayout_7")
    self.label_34 = QtWidgets.QLabel(self.layoutWidget1)
    font = QtGui.QFont()
    font.setPointSize(25)
    self.label_34.setFont(font)
    self.label_34.setAlignment(QtCore.Qt.AlignCenter)
    self.label_34.setObjectName("label_34")
    self.horizontalLayout_7.addWidget(self.label_34)
    self.label_35 = QtWidgets.QLabel(self.layoutWidget1)
    font = QtGui.QFont()
    font.setPointSize(25)
    self.label_35.setFont(font)
    self.label_35.setAlignment(QtCore.Qt.AlignCenter)
    self.label_35.setObjectName("label_35")
    self.horizontalLayout_7.addWidget(self.label_35)
    self.label_36 = QtWidgets.QLabel(self.layoutWidget1)
    font = QtGui.QFont()
    font.setPointSize(25)
    self.label_36.setFont(font)
    self.label_36.setAlignment(QtCore.Qt.AlignCenter)
    self.label_36.setObjectName("label_36")
    self.horizontalLayout_7.addWidget(self.label_36)
    self.verticalLayout_10.addLayout(self.horizontalLayout_7)
    self.label_37 = QtWidgets.QLabel(self.layoutWidget1)
    font = QtGui.QFont()
    font.setPointSize(25)
    self.label_37.setFont(font)
    self.label_37.setAlignment(QtCore.Qt.AlignCenter)
    self.label_37.setObjectName("label_37")
    self.verticalLayout_10.addWidget(self.label_37)
    self.layoutWidget2 = QtWidgets.QWidget(self.centralwidget)
    self.layoutWidget2.setGeometry(QtCore.QRect(960, 20, 254, 62))
    self.layoutWidget2.setObjectName("layoutWidget2")
    self.verticalLayout_11 = QtWidgets.QVBoxLayout(self.layoutWidget2)
    self.verticalLayout_11.setContentsMargins(0, 0, 0, 0)
    self.verticalLayout_11.setObjectName("verticalLayout_11")
    self.label_38 = QtWidgets.QLabel(self.layoutWidget2)
    font = QtGui.QFont()
    font.setPointSize(17)
    self.label_38.setFont(font)
    self.label_38.setObjectName("label_38")
    self.verticalLayout_11.addWidget(self.label_38)
    self.label_39 = QtWidgets.QLabel(self.layoutWidget2)
    font = QtGui.QFont()
    font.setPointSize(17)
    self.label_39.setFont(font)
    self.label_39.setObjectName("label_39")
    self.verticalLayout_11.addWidget(self.label_39)
    self.layoutWidget_3 = QtWidgets.QWidget(self.centralwidget)
    self.layoutWidget_3.setGeometry(QtCore.QRect(1050, 100, 236, 243))
    self.layoutWidget_3.setObjectName("layoutWidget_3")
    self.verticalLayout_14 = QtWidgets.QVBoxLayout(self.layoutWidget_3)
    self.verticalLayout_14.setContentsMargins(0, 0, 0, 0)
    self.verticalLayout_14.setObjectName("verticalLayout_14")
    self.label_43 = QtWidgets.QLabel(self.layoutWidget_3)
    font = QtGui.QFont()
    font.setPointSize(12)
    self.label_43.setFont(font)
    self.label_43.setObjectName("label_43")
    self.verticalLayout_14.addWidget(self.label_43)
    self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
    self.horizontalLayout_2.setObjectName("horizontalLayout_2")
    self.verticalLayout_3 = QtWidgets.QVBoxLayout()
    self.verticalLayout_3.setObjectName("verticalLayout_3")
    self.label_8 = QtWidgets.QLabel(self.layoutWidget_3)
    self.label_8.setObjectName("label_8")
    self.verticalLayout_3.addWidget(self.label_8)
    self.label_9 = QtWidgets.QLabel(self.layoutWidget_3)
    self.label_9.setObjectName("label_9")
    self.verticalLayout_3.addWidget(self.label_9)
    self.label_10 = QtWidgets.QLabel(self.layoutWidget_3)
    self.label_10.setObjectName("label_10")
    self.verticalLayout_3.addWidget(self.label_10)
    self.label_11 = QtWidgets.QLabel(self.layoutWidget_3)
    self.label_11.setObjectName("label_11")
    self.verticalLayout_3.addWidget(self.label_11)
    self.label_12 = QtWidgets.QLabel(self.layoutWidget_3)
    self.label_12.setObjectName("label_12")
    self.verticalLayout_3.addWidget(self.label_12)
    self.label_13 = QtWidgets.QLabel(self.layoutWidget_3)
    self.label_13.setObjectName("label_13")
    self.verticalLayout_3.addWidget(self.label_13)
    self.label_14 = QtWidgets.QLabel(self.layoutWidget_3)
    self.label_14.setObjectName("label_14")
    self.verticalLayout_3.addWidget(self.label_14)
    self.horizontalLayout_2.addLayout(self.verticalLayout_3)
    self.verticalLayout_4 = QtWidgets.QVBoxLayout()
    self.verticalLayout_4.setObjectName("verticalLayout_4")
    self.lineEdit_8 = QtWidgets.QLineEdit(self.layoutWidget_3)
    self.lineEdit_8.setAlignment(QtCore.Qt.AlignCenter)
    self.lineEdit_8.setObjectName("lineEdit_8")
    self.verticalLayout_4.addWidget(self.lineEdit_8)
    self.lineEdit_9 = QtWidgets.QLineEdit(self.layoutWidget_3)
    self.lineEdit_9.setAlignment(QtCore.Qt.AlignCenter)
    self.lineEdit_9.setObjectName("lineEdit_9")
    self.verticalLayout_4.addWidget(self.lineEdit_9)
    self.lineEdit_10 = QtWidgets.QLineEdit(self.layoutWidget_3)
    self.lineEdit_10.setAlignment(QtCore.Qt.AlignCenter)
    self.lineEdit_10.setObjectName("lineEdit_10")
    self.verticalLayout_4.addWidget(self.lineEdit_10)
    self.lineEdit_11 = QtWidgets.QLineEdit(self.layoutWidget_3)
    self.lineEdit_11.setAlignment(QtCore.Qt.AlignCenter)
    self.lineEdit_11.setObjectName("lineEdit_11")
    self.verticalLayout_4.addWidget(self.lineEdit_11)
    self.lineEdit_12 = QtWidgets.QLineEdit(self.layoutWidget_3)
    self.lineEdit_12.setAlignment(QtCore.Qt.AlignCenter)
    self.lineEdit_12.setObjectName("lineEdit_12")
    self.verticalLayout_4.addWidget(self.lineEdit_12)
    self.lineEdit_13 = QtWidgets.QLineEdit(self.layoutWidget_3)
    self.lineEdit_13.setAlignment(QtCore.Qt.AlignCenter)
    self.lineEdit_13.setObjectName("lineEdit_13")
    self.verticalLayout_4.addWidget(self.lineEdit_13)
    self.lineEdit_14 = QtWidgets.QLineEdit(self.layoutWidget_3)
    self.lineEdit_14.setAlignment(QtCore.Qt.AlignCenter)
    self.lineEdit_14.setObjectName("lineEdit_14")
    self.verticalLayout_4.addWidget(self.lineEdit_14)
    self.horizontalLayout_2.addLayout(self.verticalLayout_4)
    self.verticalLayout_14.addLayout(self.horizontalLayout_2)
    self.layoutWidget_4 = QtWidgets.QWidget(self.centralwidget)
    self.layoutWidget_4.setGeometry(QtCore.QRect(1050, 380, 236, 243))
    self.layoutWidget_4.setObjectName("layoutWidget_4")
    self.verticalLayout_15 = QtWidgets.QVBoxLayout(self.layoutWidget_4)
    self.verticalLayout_15.setContentsMargins(0, 0, 0, 0)
    self.verticalLayout_15.setObjectName("verticalLayout_15")
    self.label_44 = QtWidgets.QLabel(self.layoutWidget_4)
    font = QtGui.QFont()
    font.setPointSize(12)
    self.label_44.setFont(font)
    self.label_44.setObjectName("label_44")
    self.verticalLayout_15.addWidget(self.label_44)
    self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
    self.horizontalLayout_4.setObjectName("horizontalLayout_4")
    self.verticalLayout_7 = QtWidgets.QVBoxLayout()
    self.verticalLayout_7.setObjectName("verticalLayout_7")
    self.label_22 = QtWidgets.QLabel(self.layoutWidget_4)
    self.label_22.setObjectName("label_22")
    self.verticalLayout_7.addWidget(self.label_22)
    self.label_23 = QtWidgets.QLabel(self.layoutWidget_4)
    self.label_23.setObjectName("label_23")
    self.verticalLayout_7.addWidget(self.label_23)
    self.label_24 = QtWidgets.QLabel(self.layoutWidget_4)
    self.label_24.setObjectName("label_24")
    self.verticalLayout_7.addWidget(self.label_24)
    self.label_25 = QtWidgets.QLabel(self.layoutWidget_4)
    self.label_25.setObjectName("label_25")
    self.verticalLayout_7.addWidget(self.label_25)
    self.label_26 = QtWidgets.QLabel(self.layoutWidget_4)
    self.label_26.setObjectName("label_26")
    self.verticalLayout_7.addWidget(self.label_26)
    self.label_27 = QtWidgets.QLabel(self.layoutWidget_4)
    self.label_27.setObjectName("label_27")
    self.verticalLayout_7.addWidget(self.label_27)
    self.label_28 = QtWidgets.QLabel(self.layoutWidget_4)
    self.label_28.setObjectName("label_28")
    self.verticalLayout_7.addWidget(self.label_28)
    self.horizontalLayout_4.addLayout(self.verticalLayout_7)
    self.verticalLayout_8 = QtWidgets.QVBoxLayout()
    self.verticalLayout_8.setObjectName("verticalLayout_8")
    self.lineEdit_22 = QtWidgets.QLineEdit(self.layoutWidget_4)
    self.lineEdit_22.setAlignment(QtCore.Qt.AlignCenter)
    self.lineEdit_22.setObjectName("lineEdit_22")
    self.verticalLayout_8.addWidget(self.lineEdit_22)
    self.lineEdit_23 = QtWidgets.QLineEdit(self.layoutWidget_4)
    self.lineEdit_23.setAlignment(QtCore.Qt.AlignCenter)
    self.lineEdit_23.setObjectName("lineEdit_23")
    self.verticalLayout_8.addWidget(self.lineEdit_23)
    self.lineEdit_24 = QtWidgets.QLineEdit(self.layoutWidget_4)
    self.lineEdit_24.setAlignment(QtCore.Qt.AlignCenter)
    self.lineEdit_24.setObjectName("lineEdit_24")
    self.verticalLayout_8.addWidget(self.lineEdit_24)
    self.lineEdit_25 = QtWidgets.QLineEdit(self.layoutWidget_4)
    self.lineEdit_25.setAlignment(QtCore.Qt.AlignCenter)
    self.lineEdit_25.setObjectName("lineEdit_25")
    self.verticalLayout_8.addWidget(self.lineEdit_25)
    self.lineEdit_26 = QtWidgets.QLineEdit(self.layoutWidget_4)
    self.lineEdit_26.setAlignment(QtCore.Qt.AlignCenter)
    self.lineEdit_26.setObjectName("lineEdit_26")
    self.verticalLayout_8.addWidget(self.lineEdit_26)
    self.lineEdit_27 = QtWidgets.QLineEdit(self.layoutWidget_4)
    self.lineEdit_27.setAlignment(QtCore.Qt.AlignCenter)
    self.lineEdit_27.setObjectName("lineEdit_27")
    self.verticalLayout_8.addWidget(self.lineEdit_27)
    self.lineEdit_28 = QtWidgets.QLineEdit(self.layoutWidget_4)
    self.lineEdit_28.setAlignment(QtCore.Qt.AlignCenter)
    self.lineEdit_28.setObjectName("lineEdit_28")
    self.verticalLayout_8.addWidget(self.lineEdit_28)
    self.horizontalLayout_4.addLayout(self.verticalLayout_8)
    self.verticalLayout_15.addLayout(self.horizontalLayout_4)
    self.widget = QtWidgets.QWidget(self.centralwidget)
    self.widget.setGeometry(QtCore.QRect(10, 100, 236, 243))
    self.widget.setObjectName("widget")
    self.verticalLayout_12 = QtWidgets.QVBoxLayout(self.widget)
    self.verticalLayout_12.setContentsMargins(0, 0, 0, 0)
    self.verticalLayout_12.setObjectName("verticalLayout_12")
    self.label_41 = QtWidgets.QLabel(self.widget)
    font = QtGui.QFont()
    font.setPointSize(12)
    self.label_41.setFont(font)
    self.label_41.setObjectName("label_41")
    self.verticalLayout_12.addWidget(self.label_41)
    self.horizontalLayout = QtWidgets.QHBoxLayout()
    self.horizontalLayout.setObjectName("horizontalLayout")
    self.verticalLayout_2 = QtWidgets.QVBoxLayout()
    self.verticalLayout_2.setObjectName("verticalLayout_2")
    self.label = QtWidgets.QLabel(self.widget)
    self.label.setObjectName("label")
    self.verticalLayout_2.addWidget(self.label)
    self.label_2 = QtWidgets.QLabel(self.widget)
    self.label_2.setObjectName("label_2")
    self.verticalLayout_2.addWidget(self.label_2)
    self.label_3 = QtWidgets.QLabel(self.widget)
    self.label_3.setObjectName("label_3")
    self.verticalLayout_2.addWidget(self.label_3)
    self.label_4 = QtWidgets.QLabel(self.widget)
    self.label_4.setObjectName("label_4")
    self.verticalLayout_2.addWidget(self.label_4)
    self.label_5 = QtWidgets.QLabel(self.widget)
    self.label_5.setObjectName("label_5")
    self.verticalLayout_2.addWidget(self.label_5)
    self.label_6 = QtWidgets.QLabel(self.widget)
    self.label_6.setObjectName("label_6")
    self.verticalLayout_2.addWidget(self.label_6)
    self.label_7 = QtWidgets.QLabel(self.widget)
    self.label_7.setObjectName("label_7")
    self.verticalLayout_2.addWidget(self.label_7)
    self.horizontalLayout.addLayout(self.verticalLayout_2)
    self.verticalLayout = QtWidgets.QVBoxLayout()
    self.verticalLayout.setObjectName("verticalLayout")
    self.lineEdit = QtWidgets.QLineEdit(self.widget)
    self.lineEdit.setAlignment(QtCore.Qt.AlignCenter)
    self.lineEdit.setObjectName("lineEdit")
    self.verticalLayout.addWidget(self.lineEdit)
    self.lineEdit_2 = QtWidgets.QLineEdit(self.widget)
    self.lineEdit_2.setAlignment(QtCore.Qt.AlignCenter)
    self.lineEdit_2.setObjectName("lineEdit_2")
    self.verticalLayout.addWidget(self.lineEdit_2)
    self.lineEdit_3 = QtWidgets.QLineEdit(self.widget)
    self.lineEdit_3.setAlignment(QtCore.Qt.AlignCenter)
    self.lineEdit_3.setObjectName("lineEdit_3")
    self.verticalLayout.addWidget(self.lineEdit_3)
    self.lineEdit_4 = QtWidgets.QLineEdit(self.widget)
    self.lineEdit_4.setAlignment(QtCore.Qt.AlignCenter)
    self.lineEdit_4.setObjectName("lineEdit_4")
    self.verticalLayout.addWidget(self.lineEdit_4)
    self.lineEdit_5 = QtWidgets.QLineEdit(self.widget)
    self.lineEdit_5.setAlignment(QtCore.Qt.AlignCenter)
    self.lineEdit_5.setObjectName("lineEdit_5")
    self.verticalLayout.addWidget(self.lineEdit_5)
    self.lineEdit_6 = QtWidgets.QLineEdit(self.widget)
    self.lineEdit_6.setAlignment(QtCore.Qt.AlignCenter)
    self.lineEdit_6.setObjectName("lineEdit_6")
    self.verticalLayout.addWidget(self.lineEdit_6)
    self.lineEdit_7 = QtWidgets.QLineEdit(self.widget)
    self.lineEdit_7.setAlignment(QtCore.Qt.AlignCenter)
    self.lineEdit_7.setObjectName("lineEdit_7")
    self.verticalLayout.addWidget(self.lineEdit_7)
    self.horizontalLayout.addLayout(self.verticalLayout)
    self.verticalLayout_12.addLayout(self.horizontalLayout)
    self.widget1 = QtWidgets.QWidget(self.centralwidget)
    self.widget1.setGeometry(QtCore.QRect(10, 380, 236, 242))
    self.widget1.setObjectName("widget1")
    self.verticalLayout_13 = QtWidgets.QVBoxLayout(self.widget1)
    self.verticalLayout_13.setContentsMargins(0, 0, 0, 0)
    self.verticalLayout_13.setObjectName("verticalLayout_13")
    self.label_42 = QtWidgets.QLabel(self.widget1)
    font = QtGui.QFont()
    font.setPointSize(12)
    self.label_42.setFont(font)
    self.label_42.setObjectName("label_42")
    self.verticalLayout_13.addWidget(self.label_42)
    self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
    self.horizontalLayout_3.setObjectName("horizontalLayout_3")
    self.verticalLayout_5 = QtWidgets.QVBoxLayout()
    self.verticalLayout_5.setObjectName("verticalLayout_5")
    self.label_15 = QtWidgets.QLabel(self.widget1)
    self.label_15.setObjectName("label_15")
    self.verticalLayout_5.addWidget(self.label_15)
    self.label_16 = QtWidgets.QLabel(self.widget1)
    self.label_16.setObjectName("label_16")
    self.verticalLayout_5.addWidget(self.label_16)
    self.label_17 = QtWidgets.QLabel(self.widget1)
    self.label_17.setObjectName("label_17")
    self.verticalLayout_5.addWidget(self.label_17)
    self.label_18 = QtWidgets.QLabel(self.widget1)
    self.label_18.setObjectName("label_18")
    self.verticalLayout_5.addWidget(self.label_18)
    self.label_19 = QtWidgets.QLabel(self.widget1)
    self.label_19.setObjectName("label_19")
    self.verticalLayout_5.addWidget(self.label_19)
    self.label_20 = QtWidgets.QLabel(self.widget1)
    self.label_20.setObjectName("label_20")
    self.verticalLayout_5.addWidget(self.label_20)
    self.label_21 = QtWidgets.QLabel(self.widget1)
    self.label_21.setObjectName("label_21")
    self.verticalLayout_5.addWidget(self.label_21)
    self.horizontalLayout_3.addLayout(self.verticalLayout_5)
    self.verticalLayout_6 = QtWidgets.QVBoxLayout()
    self.verticalLayout_6.setObjectName("verticalLayout_6")
    self.lineEdit_15 = QtWidgets.QLineEdit(self.widget1)
    self.lineEdit_15.setAlignment(QtCore.Qt.AlignCenter)
    self.lineEdit_15.setObjectName("lineEdit_15")
    self.verticalLayout_6.addWidget(self.lineEdit_15)
    self.lineEdit_16 = QtWidgets.QLineEdit(self.widget1)
    self.lineEdit_16.setAlignment(QtCore.Qt.AlignCenter)
    self.lineEdit_16.setObjectName("lineEdit_16")
    self.verticalLayout_6.addWidget(self.lineEdit_16)
    self.lineEdit_17 = QtWidgets.QLineEdit(self.widget1)
    self.lineEdit_17.setAlignment(QtCore.Qt.AlignCenter)
    self.lineEdit_17.setObjectName("lineEdit_17")
    self.verticalLayout_6.addWidget(self.lineEdit_17)
    self.lineEdit_18 = QtWidgets.QLineEdit(self.widget1)
    self.lineEdit_18.setAlignment(QtCore.Qt.AlignCenter)
    self.lineEdit_18.setObjectName("lineEdit_18")
    self.verticalLayout_6.addWidget(self.lineEdit_18)
    self.lineEdit_19 = QtWidgets.QLineEdit(self.widget1)
    self.lineEdit_19.setAlignment(QtCore.Qt.AlignCenter)
    self.lineEdit_19.setObjectName("lineEdit_19")
    self.verticalLayout_6.addWidget(self.lineEdit_19)
    self.lineEdit_20 = QtWidgets.QLineEdit(self.widget1)
    self.lineEdit_20.setAlignment(QtCore.Qt.AlignCenter)
    self.lineEdit_20.setObjectName("lineEdit_20")
    self.verticalLayout_6.addWidget(self.lineEdit_20)
    self.lineEdit_21 = QtWidgets.QLineEdit(self.widget1)
    self.lineEdit_21.setAlignment(QtCore.Qt.AlignCenter)
    self.lineEdit_21.setObjectName("lineEdit_21")
    self.verticalLayout_6.addWidget(self.lineEdit_21)
    self.horizontalLayout_3.addLayout(self.verticalLayout_6)
    self.verticalLayout_13.addLayout(self.horizontalLayout_3)
    MainWindow.setCentralWidget(self.centralwidget)
    self.menubar = QtWidgets.QMenuBar(MainWindow)
    self.menubar.setGeometry(QtCore.QRect(0, 0, 1336, 22))
    self.menubar.setObjectName("menubar")
    MainWindow.setMenuBar(self.menubar)
    self.statusbar = QtWidgets.QStatusBar(MainWindow)
    self.statusbar.setObjectName("statusbar")
    MainWindow.setStatusBar(self.statusbar)

    self.retranslateUi(MainWindow)
    QtCore.QMetaObject.connectSlotsByName(MainWindow)

  def retranslateUi(self, MainWindow):
    _translate = QtCore.QCoreApplication.translate
    MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
    self.pushButton.setText(_translate("MainWindow", "HOME"))
    self.pushButton_2.setText(_translate("MainWindow", "RESET"))
    self.pushButton_3.setText(_translate("MainWindow", "FULLSCREEN"))
    self.pushButton_4.setText(_translate("MainWindow", "EXIT"))
    self.label_29.setText(_translate("MainWindow", "INITIAL"))
    self.label_30.setText(_translate("MainWindow", "NORMAL"))
    self.label_33.setText(_translate("MainWindow", "ICHIRO - ITS"))
    self.label_34.setText(_translate("MainWindow", "0"))
    self.label_35.setText(_translate("MainWindow", "10:00"))
    self.label_36.setText(_translate("MainWindow", "0"))
    self.label_37.setText(_translate("MainWindow", "1"))
    self.label_38.setText(_translate("MainWindow", "INSTITUT TEKNOLOGI"))
    self.label_39.setText(_translate("MainWindow", "SEPULUH NOPEMBER"))
    self.label_43.setText(_translate("MainWindow", "#3 HIRO (INACTIVE)"))
    self.label_8.setText(_translate("MainWindow", "ROLE"))
    self.label_9.setText(_translate("MainWindow", "STATE"))
    self.label_10.setText(_translate("MainWindow", "DIRECTION"))
    self.label_11.setText(_translate("MainWindow", "SHOOT DIR"))
    self.label_12.setText(_translate("MainWindow", "POSISITION"))
    self.label_13.setText(_translate("MainWindow", "BALL POST"))
    self.label_14.setText(_translate("MainWindow", "HEAD"))
    self.lineEdit_8.setText(_translate("MainWindow", "0 - PYRG"))
    self.lineEdit_9.setText(_translate("MainWindow", "0 - JATUH"))
    self.lineEdit_10.setText(_translate("MainWindow", "0"))
    self.lineEdit_11.setText(_translate("MainWindow", "0"))
    self.lineEdit_12.setText(_translate("MainWindow", "-1, -1"))
    self.lineEdit_13.setText(_translate("MainWindow", "-1, -1"))
    self.lineEdit_14.setText(_translate("MainWindow", "0, 0"))
    self.label_44.setText(_translate("MainWindow", "#4 IHAARO (INACTIVE)"))
    self.label_22.setText(_translate("MainWindow", "ROLE"))
    self.label_23.setText(_translate("MainWindow", "STATE"))
    self.label_24.setText(_translate("MainWindow", "DIRECTION"))
    self.label_25.setText(_translate("MainWindow", "SHOOT DIR"))
    self.label_26.setText(_translate("MainWindow", "POSISITION"))
    self.label_27.setText(_translate("MainWindow", "BALL POST"))
    self.label_28.setText(_translate("MainWindow", "HEAD"))
    self.lineEdit_22.setText(_translate("MainWindow", "0 - PYRG"))
    self.lineEdit_23.setText(_translate("MainWindow", "0 - JATUH"))
    self.lineEdit_24.setText(_translate("MainWindow", "0"))
    self.lineEdit_25.setText(_translate("MainWindow", "0"))
    self.lineEdit_26.setText(_translate("MainWindow", "-1, -1"))
    self.lineEdit_27.setText(_translate("MainWindow", "-1, -1"))
    self.lineEdit_28.setText(_translate("MainWindow", "0, 0"))
    self.label_41.setText(_translate("MainWindow", "#1 ARATA (INACTIVE)"))
    self.label.setText(_translate("MainWindow", "ROLE"))
    self.label_2.setText(_translate("MainWindow", "STATE"))
    self.label_3.setText(_translate("MainWindow", "DIRECTION"))
    self.label_4.setText(_translate("MainWindow", "SHOOT DIR"))
    self.label_5.setText(_translate("MainWindow", "POSISITION"))
    self.label_6.setText(_translate("MainWindow", "BALL POST"))
    self.label_7.setText(_translate("MainWindow", "HEAD"))
    self.lineEdit.setText(_translate("MainWindow", "0 - PYRG"))
    self.lineEdit_2.setText(_translate("MainWindow", "0 - JATUH"))
    self.lineEdit_3.setText(_translate("MainWindow", "0"))
    self.lineEdit_4.setText(_translate("MainWindow", "0"))
    self.lineEdit_5.setText(_translate("MainWindow", "-1, -1"))
    self.lineEdit_6.setText(_translate("MainWindow", "-1, -1"))
    self.lineEdit_7.setText(_translate("MainWindow", "0, 0"))
    self.label_42.setText(_translate("MainWindow", "#2 TOMO (INACTIVE)"))
    self.label_15.setText(_translate("MainWindow", "ROLE"))
    self.label_16.setText(_translate("MainWindow", "STATE"))
    self.label_17.setText(_translate("MainWindow", "DIRECTION"))
    self.label_18.setText(_translate("MainWindow", "SHOOT DIR"))
    self.label_19.setText(_translate("MainWindow", "POSISITION"))
    self.label_20.setText(_translate("MainWindow", "BALL POST"))
    self.label_21.setText(_translate("MainWindow", "HEAD"))
    self.lineEdit_15.setText(_translate("MainWindow", "0 - PYRG"))
    self.lineEdit_16.setText(_translate("MainWindow", "0 - JATUH"))
    self.lineEdit_17.setText(_translate("MainWindow", "0"))
    self.lineEdit_18.setText(_translate("MainWindow", "0"))
    self.lineEdit_19.setText(_translate("MainWindow", "-1, -1"))
    self.lineEdit_20.setText(_translate("MainWindow", "-1, -1"))
    self.lineEdit_21.setText(_translate("MainWindow", "0, 0"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
