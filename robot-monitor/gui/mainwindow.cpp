#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "Field.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QPixmap pix31("gambar/Field.png");
    ui->label_31->setPixmap(pix31);
    QPixmap pix32("gambar/ichiro.png");
    ui->label_32->setPixmap(pix32);
    QPixmap pix40("gambar/Badge_ITS.png");
    ui->label_40->setPixmap(pix40);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    if (counter==0)
    {
        ui->pushButton->setText("AWAY");
        QPixmap pix311("gambar/FieldFlipped.png");
        ui->label_31->setPixmap(pix311);
        counter++;
    }
    else if(counter==1)
    {
        ui->pushButton->setText("HOME");
        QPixmap pix312("gambar/Field.png");
        ui->label_31->setPixmap(pix312);
        counter--;
    }
}

void MainWindow::on_pushButton_3_clicked()
{
    if (!flag1)
    {
        ui->pushButton_3->setText("FULLSCREEN");
        showFullScreen();
        ui->label_43->setGeometry(1,1,243,20);
        flag1 = true;
    }
    else
    {
        ui->pushButton_3->setText("WINDOW");
        showNormal();
        flag1 = false;
    }
}



void MainWindow::on_pushButton_2_clicked()
{
    ui->label_34->setText("0");
    ui->label_35->setText("10:00");
    ui->label_36->setText("0");
    ui->label_37->setText("1");

    ui->lineEdit->setText("0-PYGR");
    //ui->lineEdit->setStyleSheet("background-color: rgb(173, 127, 168)");
    ui->lineEdit_2->setText("0-JATUH");
    //ui->lineEdit_2->setStyleSheet("background-color: rgb(173, 127, 168)");
    ui->lineEdit_3->setText("0");
    ui->lineEdit_4->setText("0");
    ui->lineEdit_5->setText("-1, -1");
    ui->lineEdit_6->setText("-1, -1");
    ui->lineEdit_7->setText("0");

    ui->lineEdit_8->setText("0-PYGR");
    ui->lineEdit_9->setText("0-JATUH");
    ui->lineEdit_10->setText("0");
    ui->lineEdit_11->setText("0");
    ui->lineEdit_12->setText("-1, -1");
    ui->lineEdit_13->setText("-1, -1");
    ui->lineEdit_14->setText("0");

    ui->lineEdit_15->setText("0-PYGR");
    ui->lineEdit_16->setText("0-JATUH");
    ui->lineEdit_17->setText("0");
    ui->lineEdit_18->setText("0");
    ui->lineEdit_19->setText("-1, -1");
    ui->lineEdit_20->setText("-1, -1");
    ui->lineEdit_21->setText("0");

    ui->lineEdit_22->setText("0-PYGR");
    ui->lineEdit_23->setText("0-JATUH");
    ui->lineEdit_24->setText("0");
    ui->lineEdit_25->setText("0");
    ui->lineEdit_26->setText("-1, -1");
    ui->lineEdit_27->setText("-1, -1");
    ui->lineEdit_28->setText("0");

    ui->label_29->setText("INITIAL");
    ui->label_30->setText("NORMAL");

    ui->label_41->setText("#1 ARATA (INACTIVE)");
    ui->label_42->setText("#2 TOMO (INACTIVE)");
    ui->label_43->setText("#3 HIRO (INACTIVE)");
    ui->label_44->setText("#4 ITHAARO (INACTIVE)");
}
