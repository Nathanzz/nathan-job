/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QLabel *label_31;
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout_9;
    QHBoxLayout *horizontalLayout_6;
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QPushButton *pushButton_3;
    QPushButton *pushButton_4;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label_29;
    QLabel *label_30;
    QLabel *label_33;
    QLabel *label_40;
    QWidget *layoutWidget1;
    QVBoxLayout *verticalLayout_10;
    QHBoxLayout *horizontalLayout_7;
    QLabel *label_34;
    QLabel *label_35;
    QLabel *label_36;
    QLabel *label_37;
    QWidget *layoutWidget2;
    QVBoxLayout *verticalLayout_11;
    QLabel *label_38;
    QLabel *label_39;
    QWidget *layoutWidget_3;
    QVBoxLayout *verticalLayout_14;
    QLabel *label_43;
    QHBoxLayout *horizontalLayout_2;
    QVBoxLayout *verticalLayout_3;
    QLabel *label_8;
    QLabel *label_9;
    QLabel *label_10;
    QLabel *label_11;
    QLabel *label_12;
    QLabel *label_13;
    QLabel *label_14;
    QVBoxLayout *verticalLayout_4;
    QLineEdit *lineEdit_8;
    QLineEdit *lineEdit_9;
    QLineEdit *lineEdit_10;
    QLineEdit *lineEdit_11;
    QLineEdit *lineEdit_12;
    QLineEdit *lineEdit_13;
    QLineEdit *lineEdit_14;
    QWidget *layoutWidget_4;
    QVBoxLayout *verticalLayout_15;
    QLabel *label_44;
    QHBoxLayout *horizontalLayout_4;
    QVBoxLayout *verticalLayout_7;
    QLabel *label_22;
    QLabel *label_23;
    QLabel *label_24;
    QLabel *label_25;
    QLabel *label_26;
    QLabel *label_27;
    QLabel *label_28;
    QVBoxLayout *verticalLayout_8;
    QLineEdit *lineEdit_22;
    QLineEdit *lineEdit_23;
    QLineEdit *lineEdit_24;
    QLineEdit *lineEdit_25;
    QLineEdit *lineEdit_26;
    QLineEdit *lineEdit_27;
    QLineEdit *lineEdit_28;
    QWidget *layoutWidget3;
    QVBoxLayout *verticalLayout_12;
    QLabel *label_41;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout_2;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_5;
    QLabel *label_6;
    QLabel *label_7;
    QVBoxLayout *verticalLayout;
    QLineEdit *lineEdit;
    QLineEdit *lineEdit_2;
    QLineEdit *lineEdit_3;
    QLineEdit *lineEdit_4;
    QLineEdit *lineEdit_5;
    QLineEdit *lineEdit_6;
    QLineEdit *lineEdit_7;
    QWidget *layoutWidget4;
    QVBoxLayout *verticalLayout_13;
    QLabel *label_42;
    QHBoxLayout *horizontalLayout_3;
    QVBoxLayout *verticalLayout_5;
    QLabel *label_15;
    QLabel *label_16;
    QLabel *label_17;
    QLabel *label_18;
    QLabel *label_19;
    QLabel *label_20;
    QLabel *label_21;
    QVBoxLayout *verticalLayout_6;
    QLineEdit *lineEdit_15;
    QLineEdit *lineEdit_16;
    QLineEdit *lineEdit_17;
    QLineEdit *lineEdit_18;
    QLineEdit *lineEdit_19;
    QLineEdit *lineEdit_20;
    QLineEdit *lineEdit_21;
    QLabel *label_32;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(1336, 768);
        QIcon icon;
        icon.addFile(QStringLiteral("gambar/ichiro.png"), QSize(), QIcon::Normal, QIcon::Off);
        MainWindow->setWindowIcon(icon);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        label_31 = new QLabel(centralwidget);
        label_31->setObjectName(QStringLiteral("label_31"));
        label_31->setGeometry(QRect(260, 70, 781, 561));
        label_31->setPixmap(QPixmap(QString::fromUtf8("gambar/Field.png")));
        layoutWidget = new QWidget(centralwidget);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(440, 620, 421, 75));
        verticalLayout_9 = new QVBoxLayout(layoutWidget);
        verticalLayout_9->setObjectName(QStringLiteral("verticalLayout_9"));
        verticalLayout_9->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        pushButton = new QPushButton(layoutWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        horizontalLayout_6->addWidget(pushButton);

        pushButton_2 = new QPushButton(layoutWidget);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));

        horizontalLayout_6->addWidget(pushButton_2);

        pushButton_3 = new QPushButton(layoutWidget);
        pushButton_3->setObjectName(QStringLiteral("pushButton_3"));

        horizontalLayout_6->addWidget(pushButton_3);

        pushButton_4 = new QPushButton(layoutWidget);
        pushButton_4->setObjectName(QStringLiteral("pushButton_4"));

        horizontalLayout_6->addWidget(pushButton_4);


        verticalLayout_9->addLayout(horizontalLayout_6);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        label_29 = new QLabel(layoutWidget);
        label_29->setObjectName(QStringLiteral("label_29"));
        QFont font;
        font.setPointSize(25);
        label_29->setFont(font);

        horizontalLayout_5->addWidget(label_29);

        label_30 = new QLabel(layoutWidget);
        label_30->setObjectName(QStringLiteral("label_30"));
        label_30->setFont(font);

        horizontalLayout_5->addWidget(label_30);


        verticalLayout_9->addLayout(horizontalLayout_5);

        label_33 = new QLabel(centralwidget);
        label_33->setObjectName(QStringLiteral("label_33"));
        label_33->setGeometry(QRect(120, 30, 161, 31));
        QFont font1;
        font1.setPointSize(20);
        label_33->setFont(font1);
        label_40 = new QLabel(centralwidget);
        label_40->setObjectName(QStringLiteral("label_40"));
        label_40->setGeometry(QRect(1206, 20, 61, 51));
        label_40->setPixmap(QPixmap(QString::fromUtf8("gambar/Badge_ITS.png")));
        label_40->setScaledContents(true);
        layoutWidget1 = new QWidget(centralwidget);
        layoutWidget1->setObjectName(QStringLiteral("layoutWidget1"));
        layoutWidget1->setGeometry(QRect(520, 10, 251, 86));
        verticalLayout_10 = new QVBoxLayout(layoutWidget1);
        verticalLayout_10->setObjectName(QStringLiteral("verticalLayout_10"));
        verticalLayout_10->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QStringLiteral("horizontalLayout_7"));
        label_34 = new QLabel(layoutWidget1);
        label_34->setObjectName(QStringLiteral("label_34"));
        label_34->setFont(font);
        label_34->setAlignment(Qt::AlignCenter);

        horizontalLayout_7->addWidget(label_34);

        label_35 = new QLabel(layoutWidget1);
        label_35->setObjectName(QStringLiteral("label_35"));
        label_35->setFont(font);
        label_35->setAlignment(Qt::AlignCenter);

        horizontalLayout_7->addWidget(label_35);

        label_36 = new QLabel(layoutWidget1);
        label_36->setObjectName(QStringLiteral("label_36"));
        label_36->setFont(font);
        label_36->setAlignment(Qt::AlignCenter);

        horizontalLayout_7->addWidget(label_36);


        verticalLayout_10->addLayout(horizontalLayout_7);

        label_37 = new QLabel(layoutWidget1);
        label_37->setObjectName(QStringLiteral("label_37"));
        label_37->setFont(font);
        label_37->setAlignment(Qt::AlignCenter);

        verticalLayout_10->addWidget(label_37);

        layoutWidget2 = new QWidget(centralwidget);
        layoutWidget2->setObjectName(QStringLiteral("layoutWidget2"));
        layoutWidget2->setGeometry(QRect(960, 20, 254, 62));
        verticalLayout_11 = new QVBoxLayout(layoutWidget2);
        verticalLayout_11->setObjectName(QStringLiteral("verticalLayout_11"));
        verticalLayout_11->setContentsMargins(0, 0, 0, 0);
        label_38 = new QLabel(layoutWidget2);
        label_38->setObjectName(QStringLiteral("label_38"));
        QFont font2;
        font2.setPointSize(17);
        label_38->setFont(font2);

        verticalLayout_11->addWidget(label_38);

        label_39 = new QLabel(layoutWidget2);
        label_39->setObjectName(QStringLiteral("label_39"));
        label_39->setFont(font2);

        verticalLayout_11->addWidget(label_39);

        layoutWidget_3 = new QWidget(centralwidget);
        layoutWidget_3->setObjectName(QStringLiteral("layoutWidget_3"));
        layoutWidget_3->setGeometry(QRect(1050, 100, 236, 243));
        verticalLayout_14 = new QVBoxLayout(layoutWidget_3);
        verticalLayout_14->setObjectName(QStringLiteral("verticalLayout_14"));
        verticalLayout_14->setContentsMargins(0, 0, 0, 0);
        label_43 = new QLabel(layoutWidget_3);
        label_43->setObjectName(QStringLiteral("label_43"));
        QFont font3;
        font3.setPointSize(12);
        label_43->setFont(font3);

        verticalLayout_14->addWidget(label_43);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        label_8 = new QLabel(layoutWidget_3);
        label_8->setObjectName(QStringLiteral("label_8"));

        verticalLayout_3->addWidget(label_8);

        label_9 = new QLabel(layoutWidget_3);
        label_9->setObjectName(QStringLiteral("label_9"));

        verticalLayout_3->addWidget(label_9);

        label_10 = new QLabel(layoutWidget_3);
        label_10->setObjectName(QStringLiteral("label_10"));

        verticalLayout_3->addWidget(label_10);

        label_11 = new QLabel(layoutWidget_3);
        label_11->setObjectName(QStringLiteral("label_11"));

        verticalLayout_3->addWidget(label_11);

        label_12 = new QLabel(layoutWidget_3);
        label_12->setObjectName(QStringLiteral("label_12"));

        verticalLayout_3->addWidget(label_12);

        label_13 = new QLabel(layoutWidget_3);
        label_13->setObjectName(QStringLiteral("label_13"));

        verticalLayout_3->addWidget(label_13);

        label_14 = new QLabel(layoutWidget_3);
        label_14->setObjectName(QStringLiteral("label_14"));

        verticalLayout_3->addWidget(label_14);


        horizontalLayout_2->addLayout(verticalLayout_3);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        lineEdit_8 = new QLineEdit(layoutWidget_3);
        lineEdit_8->setObjectName(QStringLiteral("lineEdit_8"));
        lineEdit_8->setAlignment(Qt::AlignCenter);

        verticalLayout_4->addWidget(lineEdit_8);

        lineEdit_9 = new QLineEdit(layoutWidget_3);
        lineEdit_9->setObjectName(QStringLiteral("lineEdit_9"));
        lineEdit_9->setAlignment(Qt::AlignCenter);

        verticalLayout_4->addWidget(lineEdit_9);

        lineEdit_10 = new QLineEdit(layoutWidget_3);
        lineEdit_10->setObjectName(QStringLiteral("lineEdit_10"));
        lineEdit_10->setAlignment(Qt::AlignCenter);

        verticalLayout_4->addWidget(lineEdit_10);

        lineEdit_11 = new QLineEdit(layoutWidget_3);
        lineEdit_11->setObjectName(QStringLiteral("lineEdit_11"));
        lineEdit_11->setAlignment(Qt::AlignCenter);

        verticalLayout_4->addWidget(lineEdit_11);

        lineEdit_12 = new QLineEdit(layoutWidget_3);
        lineEdit_12->setObjectName(QStringLiteral("lineEdit_12"));
        lineEdit_12->setAlignment(Qt::AlignCenter);

        verticalLayout_4->addWidget(lineEdit_12);

        lineEdit_13 = new QLineEdit(layoutWidget_3);
        lineEdit_13->setObjectName(QStringLiteral("lineEdit_13"));
        lineEdit_13->setAlignment(Qt::AlignCenter);

        verticalLayout_4->addWidget(lineEdit_13);

        lineEdit_14 = new QLineEdit(layoutWidget_3);
        lineEdit_14->setObjectName(QStringLiteral("lineEdit_14"));
        lineEdit_14->setAlignment(Qt::AlignCenter);

        verticalLayout_4->addWidget(lineEdit_14);


        horizontalLayout_2->addLayout(verticalLayout_4);


        verticalLayout_14->addLayout(horizontalLayout_2);

        layoutWidget_4 = new QWidget(centralwidget);
        layoutWidget_4->setObjectName(QStringLiteral("layoutWidget_4"));
        layoutWidget_4->setGeometry(QRect(1050, 380, 236, 243));
        verticalLayout_15 = new QVBoxLayout(layoutWidget_4);
        verticalLayout_15->setObjectName(QStringLiteral("verticalLayout_15"));
        verticalLayout_15->setContentsMargins(0, 0, 0, 0);
        label_44 = new QLabel(layoutWidget_4);
        label_44->setObjectName(QStringLiteral("label_44"));
        label_44->setFont(font3);

        verticalLayout_15->addWidget(label_44);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        verticalLayout_7 = new QVBoxLayout();
        verticalLayout_7->setObjectName(QStringLiteral("verticalLayout_7"));
        label_22 = new QLabel(layoutWidget_4);
        label_22->setObjectName(QStringLiteral("label_22"));

        verticalLayout_7->addWidget(label_22);

        label_23 = new QLabel(layoutWidget_4);
        label_23->setObjectName(QStringLiteral("label_23"));

        verticalLayout_7->addWidget(label_23);

        label_24 = new QLabel(layoutWidget_4);
        label_24->setObjectName(QStringLiteral("label_24"));

        verticalLayout_7->addWidget(label_24);

        label_25 = new QLabel(layoutWidget_4);
        label_25->setObjectName(QStringLiteral("label_25"));

        verticalLayout_7->addWidget(label_25);

        label_26 = new QLabel(layoutWidget_4);
        label_26->setObjectName(QStringLiteral("label_26"));

        verticalLayout_7->addWidget(label_26);

        label_27 = new QLabel(layoutWidget_4);
        label_27->setObjectName(QStringLiteral("label_27"));

        verticalLayout_7->addWidget(label_27);

        label_28 = new QLabel(layoutWidget_4);
        label_28->setObjectName(QStringLiteral("label_28"));

        verticalLayout_7->addWidget(label_28);


        horizontalLayout_4->addLayout(verticalLayout_7);

        verticalLayout_8 = new QVBoxLayout();
        verticalLayout_8->setObjectName(QStringLiteral("verticalLayout_8"));
        lineEdit_22 = new QLineEdit(layoutWidget_4);
        lineEdit_22->setObjectName(QStringLiteral("lineEdit_22"));
        lineEdit_22->setAlignment(Qt::AlignCenter);

        verticalLayout_8->addWidget(lineEdit_22);

        lineEdit_23 = new QLineEdit(layoutWidget_4);
        lineEdit_23->setObjectName(QStringLiteral("lineEdit_23"));
        lineEdit_23->setAlignment(Qt::AlignCenter);

        verticalLayout_8->addWidget(lineEdit_23);

        lineEdit_24 = new QLineEdit(layoutWidget_4);
        lineEdit_24->setObjectName(QStringLiteral("lineEdit_24"));
        lineEdit_24->setAlignment(Qt::AlignCenter);

        verticalLayout_8->addWidget(lineEdit_24);

        lineEdit_25 = new QLineEdit(layoutWidget_4);
        lineEdit_25->setObjectName(QStringLiteral("lineEdit_25"));
        lineEdit_25->setAlignment(Qt::AlignCenter);

        verticalLayout_8->addWidget(lineEdit_25);

        lineEdit_26 = new QLineEdit(layoutWidget_4);
        lineEdit_26->setObjectName(QStringLiteral("lineEdit_26"));
        lineEdit_26->setAlignment(Qt::AlignCenter);

        verticalLayout_8->addWidget(lineEdit_26);

        lineEdit_27 = new QLineEdit(layoutWidget_4);
        lineEdit_27->setObjectName(QStringLiteral("lineEdit_27"));
        lineEdit_27->setAlignment(Qt::AlignCenter);

        verticalLayout_8->addWidget(lineEdit_27);

        lineEdit_28 = new QLineEdit(layoutWidget_4);
        lineEdit_28->setObjectName(QStringLiteral("lineEdit_28"));
        lineEdit_28->setAlignment(Qt::AlignCenter);

        verticalLayout_8->addWidget(lineEdit_28);


        horizontalLayout_4->addLayout(verticalLayout_8);


        verticalLayout_15->addLayout(horizontalLayout_4);

        layoutWidget3 = new QWidget(centralwidget);
        layoutWidget3->setObjectName(QStringLiteral("layoutWidget3"));
        layoutWidget3->setGeometry(QRect(10, 100, 236, 243));
        verticalLayout_12 = new QVBoxLayout(layoutWidget3);
        verticalLayout_12->setObjectName(QStringLiteral("verticalLayout_12"));
        verticalLayout_12->setContentsMargins(0, 0, 0, 0);
        label_41 = new QLabel(layoutWidget3);
        label_41->setObjectName(QStringLiteral("label_41"));
        label_41->setFont(font3);

        verticalLayout_12->addWidget(label_41);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        label = new QLabel(layoutWidget3);
        label->setObjectName(QStringLiteral("label"));

        verticalLayout_2->addWidget(label);

        label_2 = new QLabel(layoutWidget3);
        label_2->setObjectName(QStringLiteral("label_2"));

        verticalLayout_2->addWidget(label_2);

        label_3 = new QLabel(layoutWidget3);
        label_3->setObjectName(QStringLiteral("label_3"));

        verticalLayout_2->addWidget(label_3);

        label_4 = new QLabel(layoutWidget3);
        label_4->setObjectName(QStringLiteral("label_4"));

        verticalLayout_2->addWidget(label_4);

        label_5 = new QLabel(layoutWidget3);
        label_5->setObjectName(QStringLiteral("label_5"));

        verticalLayout_2->addWidget(label_5);

        label_6 = new QLabel(layoutWidget3);
        label_6->setObjectName(QStringLiteral("label_6"));

        verticalLayout_2->addWidget(label_6);

        label_7 = new QLabel(layoutWidget3);
        label_7->setObjectName(QStringLiteral("label_7"));

        verticalLayout_2->addWidget(label_7);


        horizontalLayout->addLayout(verticalLayout_2);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        lineEdit = new QLineEdit(layoutWidget3);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(lineEdit);

        lineEdit_2 = new QLineEdit(layoutWidget3);
        lineEdit_2->setObjectName(QStringLiteral("lineEdit_2"));
        lineEdit_2->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(lineEdit_2);

        lineEdit_3 = new QLineEdit(layoutWidget3);
        lineEdit_3->setObjectName(QStringLiteral("lineEdit_3"));
        lineEdit_3->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(lineEdit_3);

        lineEdit_4 = new QLineEdit(layoutWidget3);
        lineEdit_4->setObjectName(QStringLiteral("lineEdit_4"));
        lineEdit_4->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(lineEdit_4);

        lineEdit_5 = new QLineEdit(layoutWidget3);
        lineEdit_5->setObjectName(QStringLiteral("lineEdit_5"));
        lineEdit_5->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(lineEdit_5);

        lineEdit_6 = new QLineEdit(layoutWidget3);
        lineEdit_6->setObjectName(QStringLiteral("lineEdit_6"));
        lineEdit_6->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(lineEdit_6);

        lineEdit_7 = new QLineEdit(layoutWidget3);
        lineEdit_7->setObjectName(QStringLiteral("lineEdit_7"));
        lineEdit_7->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(lineEdit_7);


        horizontalLayout->addLayout(verticalLayout);


        verticalLayout_12->addLayout(horizontalLayout);

        layoutWidget4 = new QWidget(centralwidget);
        layoutWidget4->setObjectName(QStringLiteral("layoutWidget4"));
        layoutWidget4->setGeometry(QRect(10, 380, 236, 242));
        verticalLayout_13 = new QVBoxLayout(layoutWidget4);
        verticalLayout_13->setObjectName(QStringLiteral("verticalLayout_13"));
        verticalLayout_13->setContentsMargins(0, 0, 0, 0);
        label_42 = new QLabel(layoutWidget4);
        label_42->setObjectName(QStringLiteral("label_42"));
        label_42->setFont(font3);

        verticalLayout_13->addWidget(label_42);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        label_15 = new QLabel(layoutWidget4);
        label_15->setObjectName(QStringLiteral("label_15"));

        verticalLayout_5->addWidget(label_15);

        label_16 = new QLabel(layoutWidget4);
        label_16->setObjectName(QStringLiteral("label_16"));

        verticalLayout_5->addWidget(label_16);

        label_17 = new QLabel(layoutWidget4);
        label_17->setObjectName(QStringLiteral("label_17"));

        verticalLayout_5->addWidget(label_17);

        label_18 = new QLabel(layoutWidget4);
        label_18->setObjectName(QStringLiteral("label_18"));

        verticalLayout_5->addWidget(label_18);

        label_19 = new QLabel(layoutWidget4);
        label_19->setObjectName(QStringLiteral("label_19"));

        verticalLayout_5->addWidget(label_19);

        label_20 = new QLabel(layoutWidget4);
        label_20->setObjectName(QStringLiteral("label_20"));

        verticalLayout_5->addWidget(label_20);

        label_21 = new QLabel(layoutWidget4);
        label_21->setObjectName(QStringLiteral("label_21"));

        verticalLayout_5->addWidget(label_21);


        horizontalLayout_3->addLayout(verticalLayout_5);

        verticalLayout_6 = new QVBoxLayout();
        verticalLayout_6->setObjectName(QStringLiteral("verticalLayout_6"));
        lineEdit_15 = new QLineEdit(layoutWidget4);
        lineEdit_15->setObjectName(QStringLiteral("lineEdit_15"));
        lineEdit_15->setAlignment(Qt::AlignCenter);

        verticalLayout_6->addWidget(lineEdit_15);

        lineEdit_16 = new QLineEdit(layoutWidget4);
        lineEdit_16->setObjectName(QStringLiteral("lineEdit_16"));
        lineEdit_16->setAlignment(Qt::AlignCenter);

        verticalLayout_6->addWidget(lineEdit_16);

        lineEdit_17 = new QLineEdit(layoutWidget4);
        lineEdit_17->setObjectName(QStringLiteral("lineEdit_17"));
        lineEdit_17->setAlignment(Qt::AlignCenter);

        verticalLayout_6->addWidget(lineEdit_17);

        lineEdit_18 = new QLineEdit(layoutWidget4);
        lineEdit_18->setObjectName(QStringLiteral("lineEdit_18"));
        lineEdit_18->setAlignment(Qt::AlignCenter);

        verticalLayout_6->addWidget(lineEdit_18);

        lineEdit_19 = new QLineEdit(layoutWidget4);
        lineEdit_19->setObjectName(QStringLiteral("lineEdit_19"));
        lineEdit_19->setAlignment(Qt::AlignCenter);

        verticalLayout_6->addWidget(lineEdit_19);

        lineEdit_20 = new QLineEdit(layoutWidget4);
        lineEdit_20->setObjectName(QStringLiteral("lineEdit_20"));
        lineEdit_20->setAlignment(Qt::AlignCenter);

        verticalLayout_6->addWidget(lineEdit_20);

        lineEdit_21 = new QLineEdit(layoutWidget4);
        lineEdit_21->setObjectName(QStringLiteral("lineEdit_21"));
        lineEdit_21->setAlignment(Qt::AlignCenter);

        verticalLayout_6->addWidget(lineEdit_21);


        horizontalLayout_3->addLayout(verticalLayout_6);


        verticalLayout_13->addLayout(horizontalLayout_3);

        label_32 = new QLabel(centralwidget);
        label_32->setObjectName(QStringLiteral("label_32"));
        label_32->setGeometry(QRect(50, 20, 61, 61));
        label_32->setPixmap(QPixmap(QString::fromUtf8("gambar/ichiro.png")));
        label_32->setScaledContents(true);
        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QStringLiteral("menubar"));
        menubar->setGeometry(QRect(0, 0, 1336, 22));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QStringLiteral("statusbar"));
        MainWindow->setStatusBar(statusbar);

        retranslateUi(MainWindow);
        QObject::connect(pushButton_4, SIGNAL(clicked()), MainWindow, SLOT(close()));

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", Q_NULLPTR));
        label_31->setText(QString());
        pushButton->setText(QApplication::translate("MainWindow", "HOME", Q_NULLPTR));
        pushButton_2->setText(QApplication::translate("MainWindow", "RESET", Q_NULLPTR));
        pushButton_3->setText(QApplication::translate("MainWindow", "WINDOW", Q_NULLPTR));
        pushButton_4->setText(QApplication::translate("MainWindow", "EXIT", Q_NULLPTR));
        label_29->setText(QApplication::translate("MainWindow", "INITIAL", Q_NULLPTR));
        label_30->setText(QApplication::translate("MainWindow", "NORMAL", Q_NULLPTR));
        label_33->setText(QApplication::translate("MainWindow", "ICHIRO - ITS", Q_NULLPTR));
        label_40->setText(QString());
        label_34->setText(QApplication::translate("MainWindow", "0", Q_NULLPTR));
        label_35->setText(QApplication::translate("MainWindow", "10:00", Q_NULLPTR));
        label_36->setText(QApplication::translate("MainWindow", "0", Q_NULLPTR));
        label_37->setText(QApplication::translate("MainWindow", "1", Q_NULLPTR));
        label_38->setText(QApplication::translate("MainWindow", "INSTITUT TEKNOLOGI", Q_NULLPTR));
        label_39->setText(QApplication::translate("MainWindow", "SEPULUH NOPEMBER", Q_NULLPTR));
        label_43->setText(QApplication::translate("MainWindow", "#3 HIRO (INACTIVE)", Q_NULLPTR));
        label_8->setText(QApplication::translate("MainWindow", "ROLE", Q_NULLPTR));
        label_9->setText(QApplication::translate("MainWindow", "STATE", Q_NULLPTR));
        label_10->setText(QApplication::translate("MainWindow", "DIRECTION", Q_NULLPTR));
        label_11->setText(QApplication::translate("MainWindow", "SHOOT DIR", Q_NULLPTR));
        label_12->setText(QApplication::translate("MainWindow", "POSISITION", Q_NULLPTR));
        label_13->setText(QApplication::translate("MainWindow", "BALL POST", Q_NULLPTR));
        label_14->setText(QApplication::translate("MainWindow", "HEAD", Q_NULLPTR));
        lineEdit_8->setText(QApplication::translate("MainWindow", "0 - PYRG", Q_NULLPTR));
        lineEdit_9->setText(QApplication::translate("MainWindow", "0 - JATUH", Q_NULLPTR));
        lineEdit_10->setText(QApplication::translate("MainWindow", "0", Q_NULLPTR));
        lineEdit_11->setText(QApplication::translate("MainWindow", "0", Q_NULLPTR));
        lineEdit_12->setText(QApplication::translate("MainWindow", "-1, -1", Q_NULLPTR));
        lineEdit_13->setText(QApplication::translate("MainWindow", "-1, -1", Q_NULLPTR));
        lineEdit_14->setText(QApplication::translate("MainWindow", "0, 0", Q_NULLPTR));
        label_44->setText(QApplication::translate("MainWindow", "#4 IHAARO (INACTIVE)", Q_NULLPTR));
        label_22->setText(QApplication::translate("MainWindow", "ROLE", Q_NULLPTR));
        label_23->setText(QApplication::translate("MainWindow", "STATE", Q_NULLPTR));
        label_24->setText(QApplication::translate("MainWindow", "DIRECTION", Q_NULLPTR));
        label_25->setText(QApplication::translate("MainWindow", "SHOOT DIR", Q_NULLPTR));
        label_26->setText(QApplication::translate("MainWindow", "POSISITION", Q_NULLPTR));
        label_27->setText(QApplication::translate("MainWindow", "BALL POST", Q_NULLPTR));
        label_28->setText(QApplication::translate("MainWindow", "HEAD", Q_NULLPTR));
        lineEdit_22->setText(QApplication::translate("MainWindow", "0 - PYRG", Q_NULLPTR));
        lineEdit_23->setText(QApplication::translate("MainWindow", "0 - JATUH", Q_NULLPTR));
        lineEdit_24->setText(QApplication::translate("MainWindow", "0", Q_NULLPTR));
        lineEdit_25->setText(QApplication::translate("MainWindow", "0", Q_NULLPTR));
        lineEdit_26->setText(QApplication::translate("MainWindow", "-1, -1", Q_NULLPTR));
        lineEdit_27->setText(QApplication::translate("MainWindow", "-1, -1", Q_NULLPTR));
        lineEdit_28->setText(QApplication::translate("MainWindow", "0, 0", Q_NULLPTR));
        label_41->setText(QApplication::translate("MainWindow", "#1 ARATA (INACTIVE)", Q_NULLPTR));
        label->setText(QApplication::translate("MainWindow", "ROLE", Q_NULLPTR));
        label_2->setText(QApplication::translate("MainWindow", "STATE", Q_NULLPTR));
        label_3->setText(QApplication::translate("MainWindow", "DIRECTION", Q_NULLPTR));
        label_4->setText(QApplication::translate("MainWindow", "SHOOT DIR", Q_NULLPTR));
        label_5->setText(QApplication::translate("MainWindow", "POSISITION", Q_NULLPTR));
        label_6->setText(QApplication::translate("MainWindow", "BALL POST", Q_NULLPTR));
        label_7->setText(QApplication::translate("MainWindow", "HEAD", Q_NULLPTR));
        lineEdit->setText(QApplication::translate("MainWindow", "0 - PYRG", Q_NULLPTR));
        lineEdit_2->setText(QApplication::translate("MainWindow", "0 - JATUH", Q_NULLPTR));
        lineEdit_3->setText(QApplication::translate("MainWindow", "0", Q_NULLPTR));
        lineEdit_4->setText(QApplication::translate("MainWindow", "0", Q_NULLPTR));
        lineEdit_5->setText(QApplication::translate("MainWindow", "-1, -1", Q_NULLPTR));
        lineEdit_6->setText(QApplication::translate("MainWindow", "-1, -1", Q_NULLPTR));
        lineEdit_7->setText(QApplication::translate("MainWindow", "0, 0", Q_NULLPTR));
        label_42->setText(QApplication::translate("MainWindow", "#2 TOMO (INACTIVE)", Q_NULLPTR));
        label_15->setText(QApplication::translate("MainWindow", "ROLE", Q_NULLPTR));
        label_16->setText(QApplication::translate("MainWindow", "STATE", Q_NULLPTR));
        label_17->setText(QApplication::translate("MainWindow", "DIRECTION", Q_NULLPTR));
        label_18->setText(QApplication::translate("MainWindow", "SHOOT DIR", Q_NULLPTR));
        label_19->setText(QApplication::translate("MainWindow", "POSISITION", Q_NULLPTR));
        label_20->setText(QApplication::translate("MainWindow", "BALL POST", Q_NULLPTR));
        label_21->setText(QApplication::translate("MainWindow", "HEAD", Q_NULLPTR));
        lineEdit_15->setText(QApplication::translate("MainWindow", "0 - PYRG", Q_NULLPTR));
        lineEdit_16->setText(QApplication::translate("MainWindow", "0 - JATUH", Q_NULLPTR));
        lineEdit_17->setText(QApplication::translate("MainWindow", "0", Q_NULLPTR));
        lineEdit_18->setText(QApplication::translate("MainWindow", "0", Q_NULLPTR));
        lineEdit_19->setText(QApplication::translate("MainWindow", "-1, -1", Q_NULLPTR));
        lineEdit_20->setText(QApplication::translate("MainWindow", "-1, -1", Q_NULLPTR));
        lineEdit_21->setText(QApplication::translate("MainWindow", "0, 0", Q_NULLPTR));
        label_32->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
